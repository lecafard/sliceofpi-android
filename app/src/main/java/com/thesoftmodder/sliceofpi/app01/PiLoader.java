package com.thesoftmodder.sliceofpi.app01;

import android.content.res.AssetManager;

import java.io.IOException;
import java.io.InputStream;

/**
 * Part of Slice Of PI
 */
public class PiLoader {
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    private InputStream file;
    private String PI;
    private int loadedDigits = 0;
    private int count;
    private int digitCurrentPi;
    public int digitNumber = 0;
    public String currentDigit;

    final static private int bytesAtTime = 250;
    PiLoader(AssetManager assetManager){
        try {
            file = assetManager.open("pi.dat");
            count = file.available();

            // Load first 250 bytes (500 digits)
            load(bytesAtTime);
        }
        catch (IOException e){
            e.printStackTrace();
            throw new RuntimeException();
        }

    }
    protected void load(int n){
      int testIfOver = loadedDigits + n*2;
      if(testIfOver > count*2){
        throw new RuntimeException();
      }
      try{
          digitCurrentPi = 0;
          byte[] bytes = new byte[n];
          loadedDigits += n;
          file.read(bytes);
          PI = deserializeBin(bytes);

      }
      catch (IOException e){
          e.printStackTrace();
          throw new RuntimeException();
      }
    }
    public String next(){
        char dc =  PI.charAt(digitCurrentPi);
        digitCurrentPi+=1;
        if(digitCurrentPi==bytesAtTime*2){
            load(bytesAtTime);
        }
        digitNumber+=1;
        currentDigit = String.valueOf(dc);
        return currentDigit;
    }

    public String nthDigit(){
      return PI;
    }
    protected String deserializeBin(byte[] buf){
        char[] hexChars = new char[buf.length * 2];
        for ( int j = 0; j < buf.length; j++ ) {
            int v = buf[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
