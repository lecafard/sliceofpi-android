package com.thesoftmodder.sliceofpi.app01;

import android.content.res.AssetManager;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;



public class GameActivity extends ActionBarActivity {
    private PiLoader pi;
    private String currentPiValue = "3.";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        AssetManager assetManager = getAssets();
        pi = new PiLoader(assetManager);
        TextView tv = (TextView) findViewById(R.id.pi_value);
        tv.setText(pi.nthDigit());
        TextView currentText = (TextView) findViewById(R.id.current_value);
        currentText.setText(currentPiValue);
    }
    public void addPiDisplay(String s){
      Integer length = currentPiValue.length();
      currentPiValue += s;
      if(length >= 15) {
        currentPiValue = currentPiValue.substring(1);
      }
    };
    public void gameClick( View v ) {
        TextView nextDigitT = (TextView) findViewById(R.id.next_digit);
        Integer value = 0;
        switch (v.getId()) {
            case (R.id.number0):
                value = 0;
                break;
            case (R.id.number1):
                value = 1;
                break;
            case (R.id.number2):
                value = 2;
                break;
            case (R.id.number3):
                value = 3;
                break;
            case (R.id.number4):
                value = 4;
                break;
            case (R.id.number5):
                value = 5;
                break;
            case (R.id.number6):
                value = 6;
                break;
            case (R.id.number7):
                value = 7;
                break;
            case (R.id.number8):
                value = 8;
                break;
            case (R.id.number9):
                value = 9;
                break;
        }
        String nextDigit = pi.next();
        addPiDisplay(nextDigit);

        if(Integer.parseInt(nextDigit) == value){
            Log.v("GameActivity","pi=success");
            nextDigitT.setTextColor(Color.GREEN);
        }

        else{
            Toast.makeText(getBaseContext(), "Idiot,  #"+pi.digitNumber+" decimal place is "+nextDigit,
                    Toast.LENGTH_SHORT).show();
            Log.v("GameActivity","pi=fail");
            nextDigitT.setTextColor(Color.RED);
        }
        nextDigitT.setText("Guess decimal #"+ Integer.toString(pi.digitNumber+1));
        TextView currentText = (TextView) findViewById(R.id.current_value);
        currentText.setText(currentPiValue);
    }



}
